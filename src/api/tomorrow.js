import axios from 'axios';

const tomorrowRequest = axios.create({
    baseURL: 'http://localhost:8060'
});

export default tomorrowRequest;