export default {
    messages: {
        DEFAULT: 'Что-то пошло не так',
        NETWORK_ERROR: 'Проверьте соединение с интеренетом',
        EMPRY: 'Список пуст'
    },

    mainHeader: {
        title: 'Курс Frontend',
        date: '4кв 2020г.'
    },

    student: {
        statusActive: 'Учится',
        statusFired: 'Отчислен'
    },

    studentsList: {
        title: "Список студентов",
        buttonLabelFilteredUnActive: 'Фильтр по отчисленным',
        buttonLabelFiltered: 'Фильтр по активным',
        buttonAdd: 'Добавить студента',
        buttonLabelUnFiltered: 'Показать всех',
    },

    navigation: {
        studentsLabel: 'Студенты',
        lessonsLabel: 'Занятия',
        myProfile: 'Мой профиль'
    },

    studentForm: {
        name: 'Имя',
        status: 'Статус',
        submitButton: 'Добавить',
        errorMessage: 'Пожалуйста, введите имя студента'
    },

    studentFullForm: {
        name: 'Имя',
        city: 'Город',
        age: 'Возраст',
        status: 'Статус',
        description: 'О себе',
        changeBtn: 'Изменить',
        confirmDeleteMsg: 'Вы уверены что хотете удалить',
        emptyAttr: 'Не указано',
        notFound: 'Студент не найден',
        deletedStudentMsg: 'Студен удалён!'
    },

    lessonsList: {
        title: "Занятия",
        createLesson: 'Создать занятие'
    },

    notesList: {
        title: 'Последние новости',
        nextLesson:'Следующее занятие',
        confirmDeleteMsg: 'Вы уверены что хотите удалить заметку: ',
        heading: 'Заголовок',
        description: 'Описание',
        alertHeading: 'Введите заголовок',
        addBtn: 'Добавить'
    },

    lesson: {
        section: "Блок",
        date: 'Дата',
        theme: 'Тема',
        JS: 'JS',
        React: 'React',
        html: 'Вертска',
        lessonNotFound: 'Заниятие не найдено!',
        lessonInfoNotFound: 'Не указано',
        lessonDeletedMsg: 'Занятие удалено!',
        description: 'Описание',
        createdLessonMsg: 'Занятие добавлено!',
        alertDate:'Введите дату',
        alertTheme:'Введите тему',
        alertSection:'Выберете блок'
    },

    breadcrumbs: {
        mainPage: 'Главная',
        "/students": 'Студенты',
        "/lessons": 'Занятия',
        "/lessons/createlesson": 'Создать занятие',
        '/students/H5pQnXJ62': 'Абдумуталлов Багдаулет',
        '/students/jWZCrwkFW': 'Андреев Максим',
        '/students/gYRh3Dgvk': 'Белоусов Константин',
        '/students/eIvyORjI6': 'Гойдин Василий',
        '/lessons/asdsad': 'Настройка рабочей среды',
        '/lessons/asdsad22': 'Верстка',
        '/lessons/asdsad33': 'БЭМ и другие методологии',
        '/lessons/asdsad44': 'Адаптивная верстка'
    }
};
  