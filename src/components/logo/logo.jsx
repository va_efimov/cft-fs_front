import React from 'react';
import { Link } from 'react-router-dom';

import './logo.scss';

class Logo extends React.Component {
  state = { tasks: [], loading: false, message: null };

  render() {
    return (
      <div >
        <Link to='/'>
          <img className='logo' 
            src='https://upload.wikimedia.org/wikipedia/commons/f/fc/Cft_logo_ru.png' alt='logo'></img>
        </Link>
      </div>
    );
  }
}

export default Logo;
