import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as DeleteIcon } from '../icons/delete.svg';
import locale from '../../locale';

import './note.scss';

class Note extends React.Component {
  state = { heading: '', description: '', date: '' }

  componentDidMount() {
    this.setState({
      heading: this.props.heading,
      id: this.props.id,
      description: this.props.description,
      date: this.props.date
    });
  }

  deleteNote() {
    const { deleteNote } = this.props;
    const { id } = this.state;
    deleteNote(id);
  }

  render() {
    return (
      <li 
        className='note' 
        data-id={this.state.id}
      >
        <div className='note-info'>
          <h3 className='note-heading'> {this.state.heading} </h3>
          <div className='note-date'> {this.state.date} </div>
          <div className='note-description'> {this.state.description} </div>
        </div>
        
        <DeleteIcon
          className='note_delete-icon'
          onClick={ () => {
            if(window.confirm(`${locale.notesList.confirmDeleteMsg} ${this.state.heading}?`)) {
              this.deleteNote();
            }
          }}
        />
      </li>
    );
  }
}

Note.propTypes = {
  heading: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default Note;
