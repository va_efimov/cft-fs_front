import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';
import Locale from '../../locale';

import './student-form.scss';

class StudentForm extends React.Component {
  static propTypes = {
    addStudent: PropTypes.func.isRequired
  };

  state = { name: '', status: true };

  handleInput = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;

    if(value === "false") {
      value = false;
    } else if(value === "true") {
      value = true;
    }

    this.setState({ 
      [name]: value
    });
  };

  addStudent = () => {
    const { addStudent } = this.props;
    const { name, status } = this.state;
    name==="" ? alert(Locale.studentForm.errorMessage) :
    addStudent(name, status);
  };

  render() {
    const { name, status } = this.state;
    const locale = Locale.studentForm;

    return (
      <div className='student-form'>
        <div className='student-form_attr'>
          <label className='student-form_attr__label'>
            {locale.name}
          </label>
          <input className='student-form_attr__input' type="text" name='name'value={name} onChange={this.handleInput} />
        </div>
        <div className='student-form_attr'>
          <label className='student-form_attr__label'>
            {locale.status}
          </label>
          <select className='student-form_attr__input' name='status' value={status} onChange={this.handleInput}>
            <option value={true}>{Locale.student.statusActive}</option>
            <option value={false}>{Locale.student.statusFired}</option>
          </select>
        </div>
       
        <Button className='submit-btn' onClick={this.addStudent} label={locale.submitButton} />
      </div>
    );
  }
}

export default StudentForm;