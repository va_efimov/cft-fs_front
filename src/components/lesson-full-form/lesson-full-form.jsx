import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';
import Locale from '../../locale';

import './lesson-full-form.scss';

class LessopFullForm extends React.Component {
  static propTypes = {
    editLesson: PropTypes.func.isRequired
  };

  state = { 
    theme: this.props.lesson.theme, 
    date: this.props.lesson.date, 
    section: this.props.lesson.section, 
    description: this.props.lesson.description,
    popupIsActive: this.props.lesson.popupIsActive
  };
  
  handleInput = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;

    this.setState({ 
      [name]: value
    });
  };

  editLesson = () => {
    const { editLesson } = this.props;
    const { theme, date, section, resource, description, popupIsActive} = this.state;
    if(theme === "") {
      console.log(popupIsActive);
      alert(Locale.studentForm.errorMessage);
      this.setState({ popupIsActive: false });
      console.log(popupIsActive);
    } else {
      editLesson(theme, date, section, resource, description);
      this.setState({ popupIsActive: true });
    }
  };

  render() {
    const { theme, date, section, description } = this.state;
    
    return (
      <>
        {this.state.popupIsActive && 
          <div className='student-full-form'>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.name}
              </label>
              <input className='student-full-form_attr__input' type="text" name='theme' value={theme} onChange={this.handleInput} />
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.city}
              </label>
              <input className='student-full-form_attr__input' type="text" name='date' value={date} onChange={this.handleInput} />
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.age}
              </label>
              <select className='student-full-form_attr__input' name='section' value={section} onChange={this.handleInput} >
                <option value={'Верстка'}>Верстка</option>
                <option value={'JS'}>JS</option>
                <option value={'React'}>React</option>                
              </select>
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.description}
              </label>
              <textarea className='student-full-form_attr__textarea' type="text" name='description' value={description} onChange={this.handleInput} />
            </div>
          
            <Button className='submit-btn' 
              onClick={()=> { 
                this.editLesson();
                this.setState({ popupIsActive: false })
              }} 
              label={Locale.studentFullForm.changeBtn} 
            />
          </div>
        }
      </>
    );
  }
}

export default LessopFullForm;