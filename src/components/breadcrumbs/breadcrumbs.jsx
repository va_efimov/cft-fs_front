import React from 'react';
import { Route, Link } from 'react-router-dom';

import locale from '../../locale';

import './breadcrumbs.scss';

const Breadcrumbs = (props) => (
  <div className="breadcrumbs">
      <ul className='container'>
        <li className='breadcrumb'>
          <Link className='bread-text' to='/'>
            {locale.breadcrumbs.mainPage}
          </Link>
        </li> 
        <Route path='/:path' component={BreadcrumbsItem} />
      </ul>
  </div>
);

const BreadcrumbsItem = ({ match, ...rest }) => (
  <React.Fragment>
      <li className='breadcrumb'>
        <Link className='bread-text' to={match.url}>
          {locale.breadcrumbs[match.url]}
        </Link>
      </li> 
      <Route path={`${match.url}/:path`} component={BreadcrumbsItem} />
  </React.Fragment>
);

export default Breadcrumbs;
