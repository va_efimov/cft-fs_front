import React from 'react';

import './main.scss';
import StudentsList from '../students-list/students-list';
import FullStudent from '../full-student/full-student';
import LessonsList from '../lessons-list/lessons-list';
import { Route, Switch } from 'react-router-dom';
import NotesList from '../notes-list/notes-list';
import FullLesson from '../full-lesson/full-lesson';
import LessonCreateForm from '../lesson-create-form/lesson-create-form';



class Main extends React.Component {
  state = { students: [], loading: false, message: null };

  render() {
    return (
      <main className={'main'}>
        <Switch>
          <Route exact path='/students' component={StudentsList} />
          <Route path='/students/:studentId' component={FullStudent} />
          <Route exact path='/lessons' component={LessonsList} />
          <Route exact path='/lessons/createlesson' component={LessonCreateForm} />
          <Route exact path='/lessons/:lessonId' component={FullLesson} />
          <Route exact path='/' component={NotesList} />
        </Switch> 
      </main>
    );
  }
}

export default Main;
