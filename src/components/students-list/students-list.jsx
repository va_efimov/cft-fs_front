import React from 'react';
import classNames from 'classnames';

import Button from '../button';
import tomorrowRequest from '../../api/tomorrow';
import Message from '../message';
import Locale from '../../locale';
import Student from '../student/student';
import StudentForm from '../student-form/student-form';
import MainHeader from '../main-title/main-header';

import './students-list.scss';


class StudentsList extends React.Component {
  state = { students: [], loading: false, message: null, addStudentBtn: false, filteredList: false };

  filterActive = (alreadyFiltered) => {
    if (alreadyFiltered) {
      this.componentDidMount();
    } else {
      const activeStudents = this.state.students.filter(student => student.status);
      this.setState({ students: activeStudents }); 
    }
  };

  filterUnActive = (alreadyFiltered) => {
    if (alreadyFiltered) {
      this.componentDidMount();
    } else {
      const activeStudents = this.state.students.filter(student => !student.status);
      this.setState({ students: activeStudents }); 
    }
  };

  addStudent = (name, status) => {
    this.setState({ loading: true });
    tomorrowRequest
      .post('/students', {
        name, status
      })
      .then(response => {
        if (response.data.status === "OK") {
          this.setState(prevState => ({
            students: [...prevState.students, response.data.data]
          }));
          Locale.breadcrumbs[String(response.data.url)] = response.data.data.name;
        } else {
          this.setState({ message: response.data.message });
        }
        this.setState({ loading: false });
      });
  };

  deleteStudent = (id) => {
    tomorrowRequest.delete(`/students/${id}`);
    
    const newStudentList = this.state.students.filter((st) => {
      return st.id !== id
    });

    this.setState({ students: newStudentList });
  }

  componentDidMount() {
    this.setState({ loading: true });
    tomorrowRequest
    .get('/students')
    .then(response => {
      const students = response.data.data;
      this.setState({ students, loading: false });
    })
    .catch(() => this.setState({ message: 'NETWORK_ERROR', loading: false }));
  }

  render() {
    const locale = Locale.studentsList;
    const { loading, message } = this.state;
    
    return (
      <>
        <MainHeader title={locale.title}/> 
        <Button 
          className={'add-btn'} 
          label={ this.state.addStudentBtn ? ('- ' + locale.buttonAdd) : ('+ ' + locale.buttonAdd) } 
          onClick={ () => {
              this.setState({ addStudentBtn: !this.state.addStudentBtn })
            }
          } 
        />
        
        {this.state.addStudentBtn && (<StudentForm addStudent={this.addStudent} />)}
        {message && <Message message={message} />}
        {this.state.students.length === 0 && <div className='empty-msg'>{Locale.messages.EMPRY}</div>}

        <ul className={classNames('students-list', {'students-list__loading': loading})}>
          {this.state.students.length > 0 && 
            this.state.students.map(({ name, id, status }) => (
              <Student
                deleteStudent={this.deleteStudent}
                name={name}
                status={status}
                id={id}
                key={id}
              />
            ))}
        </ul>

        <div className='filtered-btns'>
          <Button
            className='filter-btn' 
            label={ this.state.filteredList ? locale.buttonLabelUnFiltered : locale.buttonLabelFilteredUnActive } 
            onClick={() => {
                this.filterUnActive(this.state.filteredList);
                this.setState({ filteredList: !this.state.filteredList });
              }
            }
          />

          {!this.state.filteredList &&
          <Button 
            className='filter-btn' 
            label={ this.state.filteredList ? locale.buttonLabelUnFiltered : locale.buttonLabelFiltered } 
            onClick={() => {
                this.filterActive(this.state.filteredList);
                this.setState({ filteredList: !this.state.filteredList });
              }
            }
          />
          }
        </div>
      </>
    );
  }
}

export default StudentsList;
