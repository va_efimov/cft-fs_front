import React from 'react';
import classNames from 'classnames';

import tomorrowRequest from '../../api/tomorrow';
import Lesson from '../lesson/lesson';
import Locale from '../../locale';
import Message from '../message';
import Note from '../note/note';
import MainHeader from '../main-title/main-header';
import NoteForm from '../note-form/note-form';
import Button from '../button';

import './notes-list.scss';

class NotesList extends React.Component {
  state = { 
    notes: [], 
    loading: false, 
    message: null,
    nextLesson: [],
    addNoteBtn: false,
    date: '',
  };

  addNote = (heading, description) => {
    this.setState({ loading: true });
    tomorrowRequest
      .post('/notes', {
        heading, description
      })
      .then(response => {
        if (response.data.status === "OK") {
          this.setState(prevState => ({
            notes: [response.data.data, ...prevState.notes]
          }));
        } else {
          this.setState({ message: response.data.message });
        }
        this.setState({ loading: false });
      });
  };

  deleteNote = (id) => {
    tomorrowRequest.delete(`/notes/${id}`);
    const newNotesList = this.state.notes.filter((note) => {
      return note.id !== id
    });
    this.setState({ notes: newNotesList });
  }

  componentDidMount() {
    this.setState({ loading: true });
    tomorrowRequest
    .get('/notes')
    .then(response => {
      const notes = response.data.data;
      this.setState({ notes, loading: false });
    })
    .catch(() => this.setState({ message: 'NETWORK_ERROR', loading: false }));
  }

  render() {
    const locale = Locale.notesList;
    const { loading, message } = this.state;
    
    return (
      <>
        <MainHeader title={locale.title} />

        <Button 
          className={'add-btn-note'} 
          label={ this.state.addNoteBtn ? ('-') : ('+') } 
          onClick={ () => {
              this.setState({ addNoteBtn: !this.state.addNoteBtn })
            }
          } 
        /> 
        {this.state.addNoteBtn && (<NoteForm addNote={this.addNote} />)}
        {message && <Message message={message} />}
        {this.state.notes.length === 0 && <div className='empty-msg'>{Locale.messages.EMPRY}</div>}
        
        <div className={classNames('content', {'content__loading': loading})}>
          <ul className='notes-list'>
            {this.state.notes.length > 0 && 
              this.state.notes.map(({ heading, date, description, id }) => (  
                <Note
                  deleteNote={this.deleteNote}
                  heading={heading}
                  date={date}
                  description={description}
                  id={id}
                  key={id}
                />
              ))}
          </ul>

          <div className='next-lesson'>
            <h2 className='next-lesson__title'>{locale.nextLesson}</h2>
            {this.state.nextLesson.length > 0 && 
              this.state.nextLesson.map(({ theme, date, section, id }) => (  
                <Lesson
                  theme={theme}
                  date={date}
                  section={section}
                  id={id}
                  key={id}
                />
              ))
            }
          </div>
        </div>
      </>
    );
  }
}

export default NotesList;
