import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';
import Locale from '../../locale';

import './student-full-form.scss';

class StudentFullForm extends React.Component {
  static propTypes = {
    editStudent: PropTypes.func.isRequired
  };

  state = { 
    name: this.props.student.name, 
    city: this.props.student.city, 
    status: this.props.student.status, 
    age: this.props.student.age, 
    description: this.props.student.description,
    popupIsActive: this.props.student.popupIsActive
  };
  
  handleInput = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;

    if(value === "false"){
      value = false;
    } else if(value === "true") {
      value = true;
    }

    this.setState({ 
      [name]: value
    });
  };

  editStudent = () => {
    const { editStudent } = this.props;
    const { name, status, city, age, description, popupIsActive} = this.state;
    if(name === "") {
      console.log(popupIsActive);
      alert(Locale.studentForm.errorMessage);
      this.setState({ popupIsActive: false });
      console.log(popupIsActive);
    } else {
      editStudent(name, status, city, age, description);
      this.setState({ popupIsActive: true });
    }
  };

  render() {
    const { name, status, city, age, description } = this.state;
    
    return (
      <>
        {this.state.popupIsActive && 
          <div className='student-full-form'>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.name}
              </label>
              <input className='student-full-form_attr__input' type="text" name='name' value={name} onChange={this.handleInput} />
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.city}
              </label>
              <input className='student-full-form_attr__input' type="text" name='city' value={city} onChange={this.handleInput} />
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.age}
              </label>
              <input className='student-full-form_attr__input' type="text" name='age' value={age} onChange={this.handleInput} />
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.status}
              </label>
              <select className='student-full-form_attr__select' name='status' value={status} onChange={this.handleInput}>
                <option value={true}>{Locale.student.statusActive}</option>
                <option value={false}>{Locale.student.statusFired}</option>
              </select>
            </div>
            <div className='student-full-form_attr'>
              <label className='student-full-form_attr__label'>
                {Locale.studentFullForm.description}
              </label>
              <textarea className='student-full-form_attr__textarea' type="text" name='description' value={description} onChange={this.handleInput} />
            </div>
          
            <Button className='submit-btn' 
              onClick={()=> { 
                this.editStudent();
                this.setState({ popupIsActive: false })
              }} 
              label={Locale.studentFullForm.changeBtn} 
            />
          </div>
        }
      </>
    );
  }
}

export default StudentFullForm;