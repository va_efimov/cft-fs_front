import React from 'react';
import Locale from '../../locale';
import './main-header.scss';

class MainHeader extends React.Component {
  
  render() {
    const locale = Locale.mainHeader;
    
    return (
      <header className={'main-header'}>
        <h1 className={'main-header__title'}>{locale.title}</h1>
        <p className={'main-header__date'}>{locale.date}</p>
        <h2 className={'main-header__content-name'}>{this.props.title}</h2>
      </header>
    );
  }
}

export default MainHeader;
