import React from 'react';
import tomorrowRequest from '../../api/tomorrow';
import { ReactComponent as EditIcon } from '../icons/edit.svg';
import { ReactComponent as DeleteIcon } from '../icons/delete.svg';
import StudentFullForm from '../student-full-form/student-full-form';

import './full-student.scss';
import locale from '../../locale';

class FullStudent extends React.Component {
  state = { 
    name:'', 
    city:'', 
    status:'', 
    age:'', 
    description:'', 
    photo:'', 
    popupIsActive: false 
  };

  async componentDidMount() {
    const { data } = await tomorrowRequest.get(
      `/students/${this.props.match.params.studentId}`
    );
    
    if(data.status === 'ERROR') {
      this.setState({name: ''})
    } else {
    this.setState({
      name: data.data.name,
      city: data.data.city,
      status: data.data.status,
      age: data.data.age,
      description: data.data.description
    });
  }
    
  }

  deleteStudent() {
    tomorrowRequest.delete(`/students/${this.props.match.params.studentId}`);
    alert(`${locale.studentFullForm.deletedStudentMsg}`);
    this.setState({ name : '' });
  }

  editStudent = (name, status, city, age, description) => {
    tomorrowRequest.patch(`/students/${this.props.match.params.studentId}`, { 
      name: name,
      city: city, 
      status: status,
      age: age,
      description: description
    })
    .then(response => {
      if (response.data.status === "OK") {
        this.setState({
          name: response.data.name,
          city: response.data.city,
          status: response.data.status,
          age: response.data.age,
          description: response.data.description,
          popupIsActive: false
        });
        this.componentDidMount();
      }
    })
    .catch(() => {
      this.setState({ popupIsActive: true });
    })
  }

  render() {
    if(this.state.name !== '') { 
      return (
        <>
          <div className='full-student'>
            <img className='student-photo' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANgAAADpCAMAAABx2AnXAAABvFBMVEX/////y1sREiTzs4f2u5E9PGYuLlQzIR0hFhAAAADa2tvw8vHg4uEiIUD/ylf/y1w7GBYpBgD+yE7/zlsAABf+x0smJUQAABrdk2LysYkxHx3qoG0AABT1upQeAAA4OF0OGUD+2pH+0Gr+8dj+1Hz++Or+9uP+0XH9x2ckAAAxAABBQUz+79AAAAwpGhn4vnPuqXv+3ZwiJlQrAADBezelpbKNjZV7e4JxcXr+5rn+4Kf914UmFBrZrk8UCQz+6L/4v4QAADIsMWYQDjYZAAApLmb/23njjTacnKApKjheX2itrrPpulTMoUu7lkaObzhnTypPOSQ9Kx8eDBlUQSCBZzSujEA/MRocEQ9yXSwWABYqHxI+Kh9cPC1vSzeFWkGca0u6f1jPjWJ6UzyvdU/FlHVzVkXMmnmWbld4WkejemH6wXvloHTuslzw4NH20LXrm2Gqi1eKdFVXTVQAAEnv4diNeGFvYWNMR2RwcYgAEUbAnWBdVGUdJ2hcREOSe2HHxcN+bGMKCUJ4d41NMC6cglZYV3nAsKaATTHCbSRFMkQ7IC3YhjhKS3DXcRDdtJelVBGaVimCRhA2NkKNvoc3AAAP9UlEQVR4nO2djUPTRh/HaatI2pJgQw1Coby/FbBESQoIKuqapKBzvjzdhqDo3J5nbE67KUPHqjg3fRSfjX/4ubskbdIkNZekLWz5MpAyaPPh+7vf/e5ydzQ1+fLly5cvX758+fLly5cvX758+fLly5cvX758+fLVAA1PzM71DY70U1QgQFH9I4N9c7MTw42+KlcaOjc3T8XjsViMJMmAIvAp+EI8Ts3PnRtq9BU60PC5vn4dUKUQYH/fuUPl3cB0PwmgApQVlSwK0MXI/umBRl+vPQ1MB+LWRplZFw8cfLah2RFg1UecMjgHjBuZPcgNbrgvEMPwSudbLNB3UJvbwGDcIZXCFh88iBE5MOjULK1tBw5teN49low2f5ACcmjaXRDq0OJ9ByaNzAZimHmwmqhYYLbRREhDHjQuvUBTOwCmzXqNJaM12jRgl/dYUPH5hpo2QdXALlkkNdE4rrl4rbCg4nON4pqvURiqis03BGtopGZhqIocaUBDG8YZmjgmI+tehwzUAQuh1bl4nKgTFyCra3KcqGk61CteR7KJGqdDvWJ1IxuoR94oi6xXOxuuRXVYlSxWl9w45OEQxa6oevRnte+XjSJHas813wAuQFbz6mrOUUKkKIqmAzR4d6pYjStiB4mehqKWFxZWrqwsULRTutom/SG88RdCWrl19dNrn3WevX79OrH45Y2rV5YdwZE1TSCD9rmQTbduHvvi+vWzU53HkFpa8vnFSSL/+a1lCpuNHKwd16zdQARUyytXb3xx/ayCpILJAnD/urmyjMlWu3mQIXt+getduHrjWAWUFgwIONfy6RXwvRi9YqxWwWgvEOkrn4Lo6zRQ6cFktsmbCxi21SoYz9kLRPpmwgzKCIaCkvh8wT5Z7FxNwAI2Mwd9xRiEVmCwvV3FaGq14OqznRHp5WPmZGZgLS3ETdtkZJ/3XMP2u2Z6xbSFWYG1EFdsk8W9r/MxakQrw6zA8l/at8zzmnEAw7CrZ20nD8WyFdtkMa8HnTg1h0UgWoMtfm7fMo9T/oD92Rt64QsLLkswjFgMxL21DMewW1aRaAnWsrjcIMuGMabb6Jv4YBiNzNvEaL8PA2DXrJqYNdik/YTvaV82ZB8LgK0awDo7p4DyUwRBLC7m83kj2C2cQt+7Wtj2cAWK0vZinZDn2OrttfU79+/evX//zp17t6+ttgDAvGMwD4cvWBNTy2WuqcXVtTt3Nzq6FHUo2ti4v772JbHoDMy7KSuMXA/BplSu/PqGDJROh0KhI0eOgI/p9Oamgnf3HuEsFD3L+NM4htElsPwdBBU60g3ekEIlpSFd17pKhpM8gGXTHoHRjsA6VwHVEZ1COgG2kmMY6R6A0d5wDcSxJrVVsKl7lVwVYIBsTUkhkxijTZCePIpFrEgsg+Xvdh2pDpbuuE9gVx5QHsViP+aktpLupza6uquDhTo21KSPN19F9nvBNYzJRX/WqTSx0EccA7G4irDyn2FOMnpyz93mHE4Z7AaKxc7bHwdLd1xDluVvYIJ5MquDUyciMLkInlo3NDEDWKjjHuqkMcZjsjypF/vxXlMdQE/dr8yJJmBddxAY1kQVkgeNbAj3/ooyHjt71wbYppwW8QoPKA8mhSdwV4/SV65DsMUNQxMzgqXvEviFRwCuQnV/U2kO9w6mMjWwuGngMgHbQGAEVv8MRbq/D4h/a3ZZBjPmDiNYqEMGW8Z9DQ+m4bCXWtIBBHbW2MRMwLpksAC2Y5RrMNzFRdToqHyfzx4Y6saI0VHcNRZxt1w40zhA5CkwRPkKkHWuGnOHCVh6FfTQxFehk6FRPNNcT+ng3U0nR1F5+NVZUHjYAwOlB+ACOjmKBeY6LeIVVPR5meDB1NSakcsELHQ7n1+TPzuPB+a2qMLL9qNKPf913i7YWl42DNcy1+s+sCpF8lT3kVAXLH4vTd2zCbZIhNBgOnTyFNYo2m21iNWNwUhMowmB1alv7IHdI46Bjx0dHenQeazfoduODGPSHrwaAOtCYA8Im2DrxDUZbDN0Hssxt1P4IxgvhsBkx/79HxMuM7DQNw9ksBAeWMDt5CLWoIX8thuSAbDKSQFrMNCFQbBN0MZwXsr1wAVvNIayomFyqjoYFODC7cjqCwbTIrh+bLA04DqFV5TWF0wORktZgoUwk339wUBRZdG+qoKBUhF3EFFvsABJjp7HBTs/ir/g3S0YVrpX0QKncLLiqQDWzQFFbtM9VgddQhvFAcMcsCgv4baDdrZom8IBc7SO33VJhXlHQn1ZnDbmaPGz6/sSWLefyy9rnj/MM4eTF3B/Ixp35l4BM+/OzHMHzurZMpjbgabDHVXm2cM8dzh6AddTA0POtsBR9kPR2R6guOs5bmdg5Hkzy8y4vnW2V8b19Bt+6YFEm8aid5Howe0W3NtjikwTvpljzrg8uEE26wyMNsuLpjnRGZj7ZUfYt5EUmRUfJmCOntuT20g2930YRJoUwp4ZFiA9WAHneOuiHcccPrUn68ScVYtAox8Hc5gSvVnB4ng3tzEYvQpEb7b/YS0v1ZNVZsYKLod9M5Qni0yd7w+urD/0XFhT2vrn9WYpprORiyw9mQ7M2XAFyaPFsw7rYCRKR6bzy8UGePcVsCzc5W86acm0XC6e0pvFb01ON3WrV6HJIJ7kDQ+XcWOvOtKJLmf9cp53ta/fu02o7s4XIEcrwbCnffXP590eMpcnrtDfpbVgm985P3IAysvN+e5OQqNPbcq3lhBWR4c7MM9SBxT2SrEKsJC8Aj8t7yVwCeblORFu08fJkHYLiDswb/evO5wgkEUCsFBaBevq+s7Vk3m7Y9iNZWT/ALzPHEqnO9D2nfT3/bhHJGvk9YEDjkdlZAxWrN1w244cj92RSGTE8e/Js30tqpzMEMDz0Uf65DLhYVoG++FDBGp6kIJnqzt4Ts9PiMDbkgS13Dc7gCjQz3+fhmA/fI++EolGoz9NzPWhw1gashFJI7uHecCTV1YePS6wP87IEDJY008gNaZ/KnFBzVzYLjx+tLJs/yQd0nuujw/L4MUBpCdPt5Yus2xzM/tzREfW1H0youOKSr2trb0Xt9mtpxAPPUH1rFKbQ1iqjl6AT8+ePN86urS0dBSoGYpt0oOFw8rjsMwVudgKBb8TqHmr8PhZ9aN0PC06yqp23AC98OIXGUkWAtsO6y3b2VEMU8CKF1QwRSz745NqK9U9P2hAkXX+oH7VUpUcUxtZFP24cPqSoDUsyiHHTjRrxW4/pqzQapE5ZFmcwUIvP9djqWSvmzSWRcfbTveENYZFnrcawQBawWKXHFmT81eQLLbXLm8ZuGSw5qjGslfH246PvQIPioph4RMVkaiSbZnvL/D4+ASdps2aGf3UyKXEYjFSsiyyMw7AdsKRsBqJucomViJ7amZZrGaBCGUyx0gvXDZyHU2iK+RUrigIwNNtxy8Vi1HVMNMmJmvbJIPU+CRCk8qK/tXEMLWRlcBAUPLjPXw0nCuBPbcwDPxCHhvASO9rKb2M583SW2ZcSixGI2UVQVYEOV6NxHCvJVhz0gBW+7NnK+fi6AVTw5SerKgBy52+xEXLkpuYWSSCX0hlLNb6eEWoijEn/cwcrKKRAQnjPS80YLxlEwM/90gPVovzqIzSL4ijX5hyKZa9L3M1gTb2VAOWtI7EZvaFDqyWJxBqpRsAU8+rgb0sN7KmF+NjGrCwVbJHSmlfoUYlolFD2nJ42TwS1eyRK1v2dHxsR9PEqkQi+DlN9UH21+1weA0Z/eyyBZjcyHZLVVVkp23sVbgElumtYljzdnk7ah25tGeZ0r9ZOSaTJcuOvWobO10GO1HNsGb2NxWstmeXGslUzyxzhxKNbGnoEm0DlUcJrHixCpYme9TVLySluLLonktopaFLJApKqsVS2SFctAxDJKWLrseRzpWS/2IL9YslVDIJ/ivPD4THtGBSL7j6E9aebaNRWaxOeV6vvhgZoFescoc6cik1siIAu5TTD1mqgK3QATJWl37ZqNlYgH5kmTvk5FGeH8iBUOwR1CZWtRdrlmuPxv0doQGKflIBlqy0rNTIBAimFovcRQNYxRTBY5pq4B9ZG/qksu7Qgsk9mdrIBDDQBOMWZcjSW45E1gys+e0njf2LVjNVHFMqfAWMhyNotabSRCJbKIFp2NiZhmIBhV8vfQTsd7WiglMDMtfv5SELu57dkcFOlMF6t8KN5gKa0WT8pA7sqHzlcg28Azro020y2B/leupMNvtGATuhpH/2ZcPtkhX9mVXdMgFrfvsQgbUBsOM9aHr7w38Vw9i377LZ7N4frBZs+3W00UQlFd8vHVV6ZH2LQwacQZaNQbBFZNgbpYWx97JIe2/LYNvJXKNpdJpZWgJUyQowORbPIMt6IBgBi8UP/5MjkV0/I+sNtAxytbKgd2g0SqVmjrJGMGgZ+yALKyoEhmqqvbVeORJbexW1IsdaL7ZyjaYwU2Rmi22uAEOWvc0+hHNUEAzWVB/23iqpo7Uk+OBC8uC5pSiSe80uGcHYd1kt2N4bNddrwNgLz3MHFQspvHuUrSRj72cfNuVkMAEY9mevkutVrN4LzbsHoeP6iIo6NrmRvYOlIkz3XPTd3m1dmchuJ38uNvqabSpS3H1fiklw7cls9iE3jsD4D3t7J0pcLPsyuVs80CFoUDS3K9+3RQk/+8NT5NjYzru9NzASWXR3dnfm4PTFOIoWZ3Zfv2e3X/6Zzf41Nj7e07P4197eXxcu9CZf784UDydUSZFouFh82P2BR/q6+0OxGI4erujz5cuXL1++fPny5cuXL1++fPny5cuXL1++fPn6Byr8N1UT8TdVU/BvKh/ssKkqGMPoHinvh0MKWAq8i8qX9tX/1y5JCbH0KJhigqK0HzwkksEYSWLaufZEe7A9QXBBJpFgmASRA8oIRIIgggxBiGGC2OdSDb5e21IcE7mEyPMcT/BchhdETuBTQiFcIIhMROLCYTEVDqeKKfBvnR2raAuM8YtqC5GbTXvpywpYggtmMpn2TEYiCI7hggT4bBLQ8BlBLBJSrrhPiJEwU/cmxu9L4KrFFCOCj5yUSogMI/IAQ0RfCTIpSdjnMzmRz0iZfSGVE7kUowUDsciBtwyfYgCjMNkugW+YJJgwLxRyxH6uOJkQo2Gx3mAJLiMJHC/lCjwvpLhUTspkuEKOn5QyEgwwjs8EQZSJAlHgZ3hGEAUxw7drwYJMjg9yjCgKIEGAnwpyUoERMhwMv3CmyHM5HobiZJ3BGEmQ+AKXAh85ISgJAi9xQkrgROAPoJIkITMpiDyRk7h9DjQiKbcvcQk9WAaYy/HgPTMpERInivtMQeDbE1KK4aTEZAY8GZGpe+pgxPZ9mLNBchYZ8A9MzSlwbe0pZh+mcTEVLIgiU9gvFEA2TyVSohKJ5X4MPm5n0Dt4Q3YyIEvCRpmAbTNRarsHTHLyYCrzyj+z8jjM8sEOm/4Ps3/zyWCf9i8AAAAASUVORK5CYII='
              alt="studentPhoto">
            </img>
            <div className='student-info'>           
              <h1 className='student-info__name'>{this.state.name}</h1> 
              <p className='student-info__location'>{locale.studentFullForm.city}: {this.state.city || locale.studentFullForm.emptyAttr }</p>
              <p className='student-info__age'>{locale.studentFullForm.age}: {this.state.age || locale.studentFullForm.emptyAttr }</p>
              <p className='student-info__status'>{locale.studentFullForm.status}: {this.state.status ? locale.student.statusActive : locale.student.statusFired }</p>
              <h2 className='student-info__description-title'>{locale.studentFullForm.description}:</h2>
              <p className='student-info__description'>{this.state.description || locale.studentFullForm.emptyAttr}</p>
            </div>
            <div>
              <EditIcon className='student-info__icon'
                onClick={() => {
                  this.setState({ popupIsActive: !this.state.popupIsActive }) 
                }}
              />
              {this.state.popupIsActive && (
                <StudentFullForm 
                  student={this.state} 
                  editStudent={this.editStudent} 
                />)
              }
              
              <DeleteIcon
                className='student-info__icon'
                onClick={ () => {
                  if(window.confirm(`${locale.studentFullForm.confirmDeleteMsg} ${this.state.name}?`)) {
                    this.deleteStudent();
                  }
                }}
              />
            </div>
          </div>
        </>
      );
    } else {
      return <div className='empty-msg'>{locale.studentFullForm.notFound}</div>
    }
  }
}

export default FullStudent