import React from 'react';

import Header from '../header/header';
import Main from '../main/main';
import Footer from '../footer/footer';
import Breadcrumbs from '../breadcrumbs/breadcrumbs';

import './root.scss';

const Root = () => {

  //добавить state, который передаю в breadcrumbs и main
  //или contextProvider

  return (
    <div className="page">
      <Header />
      <Breadcrumbs />
      <Main />
      <Footer />
    </div>
  )
};

export default Root;
