import React from 'react';
import PropTypes from 'prop-types';
import Locale from '../../locale';
import { withRouter } from 'react-router-dom';
import './lesson.scss';

const Lesson = props => {
  const locale = Locale.lesson;

  return (
    <li 
      className='lesson' 
      data-id={props.id}
      onClick={() => props.history.push(`/lessons/${props.id}`)}
    >
      <h3 className='lesson-theme'> {props.theme} </h3>
      <div className='lesson-date'> {props.date} </div>
      <div className='lesson-section'> {locale.section}: {props.section} </div>
    </li>
  );
};

Lesson.propTypes = {
  theme: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  section: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default withRouter(Lesson);
