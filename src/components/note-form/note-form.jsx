import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';
import locale from '../../locale';

import './note-form.scss';

class NoteForm extends React.Component {
  static propTypes = {
    addNote: PropTypes.func.isRequired
  };

  state = { 
    heading: '', 
    date: '', 
    description: '' 
  };

  handleInput = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;

    this.setState({ 
      [name]: value
    });
  };

  addNote = () => {
    const { addNote } = this.props;
    const { heading, description } = this.state;
    heading==="" ? alert(locale.notesList.alertHeading) :
    addNote(heading, description);
  };

  render() {
    const { heading, description } = this.state;

    return (
      <div className='note-form'>
        <div className='note-form_attr'>
          <label className='note-form_attr__label'>
            {locale.notesList.heading}
          </label>
          <input className='note-form_attr__input' type="text" name='heading' value={heading} onChange={this.handleInput} />
        </div>

        <div className='note-form_attr'>
          <label className='note-form_attr__label'>
            {locale.notesList.description}
          </label>
          <textarea className='note-form_attr__textarea' type="text" name='description' value={description} onChange={this.handleInput} />
        </div>

        <Button className='submit-btn' onClick={this.addNote} label={locale.notesList.addBtn} />
      </div>
    );
  }
}

export default NoteForm;