import React from 'react';
import tomorrowRequest from '../../api/tomorrow';
import { ReactComponent as EditIcon } from '../icons/edit.svg';
import { ReactComponent as DeleteIcon } from '../icons/delete.svg';
import LessonFullForm from '../lesson-full-form/lesson-full-form';
import locale from '../../locale';

import './full-lesson.scss';

class FullLesson extends React.Component {
    state = { 
        theme:'', 
        date:'', 
        section:'', 
        resource:'', 
        video:'', 
        description:'', 
        needChange: false 
    };

    async componentDidMount() {
        const { data } = await tomorrowRequest.get(
            `/lessons/${this.props.match.params.lessonId}`
        );

        if(data.status === 'ERROR') {
            this.setState({ theme: '' })
        } else {
            this.setState({
                theme: data.data.theme,
                date: data.data.date,
                section: data.data.section,
                description: data.data.description
            });
        }
    }

    editLesson = (theme, date, section, description) => {
        tomorrowRequest.patch(`/lessons/${this.props.match.params.lessonId}`, { 
          theme: theme,
          date: date, 
          section: section,
          description: description
        })
        .then(response => {
          if (response.data.status === "OK") {
            this.setState({
              theme: response.data.theme,
              date: response.data.date,
              section: response.data.section,
              description: response.data.description,
              popupIsActive: false
            });
            this.componentDidMount();
          }
        })
        .catch(() => {
          this.setState({ popupIsActive: true });
        })
      }

    deleteLesson() {
        tomorrowRequest.delete(`/lessons/${this.props.match.params.lessonId}`);
        alert(`${locale.lesson.lessonDeletedMsg}`);
        this.setState({ theme : '' });
    }

    render() {
        if(this.state.theme !== '') { 
            return (
            <>
                <div className='full-lesson'>
                    <div className='lesson-info'>           
                        <h1 className='lesson-info__name'>{this.state.theme}</h1> 
                        <p className='lesson-info__date'>{locale.lesson.date}: {this.state.date || locale.lesson.lessonInfoNotFound} </p>
                        <p className='lesson-info__block'>{locale.lesson.section}: {this.state.section || locale.lesson.lessonInfoNotFound}</p>
                        <h2 className='lesson-info__description-title'>{locale.lesson.description}</h2>
                        <p className='lesson-info__description'>{this.state.description || locale.lesson.lessonInfoNotFound} </p>
                    </div>
                    <div className='icons'>
                        <EditIcon 
                            className='lesson-info__icon'
                            onClick={() => {
                                this.setState({ popupIsActive: !this.state.popupIsActive }) 
                            }}
                        />

                        {this.state.popupIsActive && (
                            <LessonFullForm 
                                lesson={this.state} 
                                editLesson={this.editLesson} 
                            />)
                        }

                        <DeleteIcon
                            className='lesson-info__icon'
                            onClick={ () => {
                                if(window.confirm(`${locale.studentFullForm.confirmDeleteMsg} ${this.state.theme}?`)) {
                                    this.deleteLesson();
                                }
                            }}
                        />
                    </div>
                </div>
            </>
            );
        } else {
            return <div className='empty-msg'>{locale.lesson.lessonNotFound}</div>
        }
    }
}

export default FullLesson