import React from 'react';

import Navigation from '../navigation/navigation';
import Logo from '../logo/logo';
import './header.scss';



class Header extends React.Component {
  state = { tasks: [], loading: false, message: null };

  render() {
   
    return (
      <header className={'header'}>
        <div className={'header-content'}>
          <Logo />
          <Navigation />
        </div>
      </header>
    );
  }
}

export default Header;
