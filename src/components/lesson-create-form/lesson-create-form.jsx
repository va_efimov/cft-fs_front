import React from 'react';

import Button from '../button';
import Locale from '../../locale';
import tomorrowRequest from '../../api/tomorrow';
import './lesson-create-form.scss';
import MainHeader from '../main-title/main-header';
import locale from '../../locale';

class LessonCreateForm extends React.Component {
  state = { 
    theme: '', 
    date: '', 
    section: '', 
    description: '',
  };

  handleInput = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;

    this.setState({ 
      [name]: value
    });
  };

  addLesson = (theme, date, section, description) => {
    if(theme === '') {
      alert(locale.lesson.alertTheme);
    } 
    else if(date === '') {
      alert(locale.lesson.alertDate);
    } 
    else if(section === '') { 
      alert(locale.lesson.alertSection)
    } else {
      tomorrowRequest
        .post('/lessons', {
          theme, date, section, description
        })
        .then(response => {
          if (response.data.status === "OK") {
            Locale.breadcrumbs[String(response.data.url)] = response.data.data.theme;
          } else {
            this.setState({ message: response.data.message });
          }
        });

        alert(locale.lesson.createdLessonMsg);
        this.setState({     
          theme: '', 
          date: '', 
          section: '', 
          description: ''
        });
      }
  };

  render() {
    const { theme, date, section, description } = this.state;
    const locale = Locale.studentForm;

    return (
      <>
        <MainHeader title={Locale.lessonsList.createLesson}/>

        <div className='create-lesson-form'>
          <div className='lesson-form_attr'>
            <label className='lesson-form_attr__label'>
              Тема
            </label>
            <input className='lesson-form_attr__input' type="text" name='theme'value={theme} onChange={this.handleInput} />
          </div>

          <div className='lesson-form_attr'>
            <label className='lesson-form_attr__label'>
              Дата
            </label>
            <input className='lesson-form_attr__input' type="text" name='date'value={date} onChange={this.handleInput} />
          </div>

          <div className='lesson-form_attr'>
              <label className='lesson-form_attr__label'>
                Блок
              </label>
              <select className='lesson-form_attr__input' name='section' value={section} onChange={this.handleInput} >
                <option value={'Верстка'}>Верстка</option>
                <option value={'JS'}>JS</option>
                <option value={'React'}>React</option>                
              </select>
          </div>

          <div className='lesson-form_attr'>
            <label className='lesson-form_attr__label'>
              Описание
            </label>
            <textarea className='lesson-form_attr__textarea' type="text" name='description'value={description} onChange={this.handleInput} />
          </div>
        
          <Button 
            className='submit-btn' 
            onClick={() => {this.addLesson(theme, date, section, description)}} 
            label={locale.submitButton} 
          />
        </div>
      </>
    );
  }
}

export default LessonCreateForm;