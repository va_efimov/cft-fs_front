import React from 'react';

import classNames from 'classnames';
import tomorrowRequest from '../../api/tomorrow';
import Locale from '../../locale';
import Message from '../message';
import Lesson from '../lesson/lesson';
import MainHeader from '../main-title/main-header';

import './lessons-list.scss';
import { Link } from 'react-router-dom';

class LessonsList extends React.Component {
  state = { lessons: [], loading: false, message: null };

  componentDidMount() {
    this.setState({ loading: true });
    tomorrowRequest
    .get('/lessons')
    .then(response => {
      const lessons = response.data.data;
      this.setState({ lessons, loading: false });
    })
    .catch(() => this.setState({ message: 'NETWORK_ERROR', loading: false }));
  }

  render() {
    const locale = Locale.lessonsList;
    const { loading, message } = this.state;
    
    return (
      <>
        <MainHeader title={locale.title} /> 
        {message && <Message message={message} />}
        <Link className='add-lesson-btn' to='/lessons/createlesson'> + </Link>

        <ul className={classNames('lessons-list', {'lessons-list__loading': loading})}>
          {this.state.lessons.length > 0 && 
            this.state.lessons.map(({ theme, date, section, id }) => (  
              <Lesson
                theme={theme}
                date={date}
                section={section}
                id={id}
                key={id}
              />
            ))}
        </ul>
      </>
    );
  }
}

export default LessonsList;
