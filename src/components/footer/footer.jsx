import React from 'react';

import './footer.scss';



class Footer extends React.Component {
  state = { tasks: [], loading: false, message: null };

  render() {
    return (
      <footer className={'footer'}>
        (c) Vladimir Efimov <br/>
        CFT Focus Start, 2020 - 2021
      </footer>
    );
  }
}

export default Footer;
