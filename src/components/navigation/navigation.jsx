import React from 'react';
import Locale from '../../locale';
import { Link } from 'react-router-dom';

import './navigation.scss';




class Navigation extends React.Component {
  render() {
    const locale = Locale.navigation;
    
    return (
      <nav className='navigation'>
        <Link to='/students' className='navigation-point'>{locale.studentsLabel}</Link>
        <Link to='/lessons' className='navigation-point'>{locale.lessonsLabel}</Link>
        <div className='navigation-point'>{locale.myProfile}</div>
      </nav>
    );
  }
}

export default Navigation;
