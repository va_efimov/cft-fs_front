import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { ReactComponent as DeleteIcon } from '../icons/delete.svg';
import Locale from '../../locale';

import './student.scss';
import locale from '../../locale';

class Student extends React.Component {
  state = { name: '', id: '', status: '' };
  
  componentDidMount() {
    this.setState({
      name: this.props.name,
      id: this.props.id,
      status: this.props.status
    });
  }

  deleteStudent() {
    const { deleteStudent } = this.props;
    const { id } = this.state;
    deleteStudent(id);
  }

  render() {
    return (
      <li className='student'
          onClick={() => this.props.history.push(`/students/${this.state.id}`)}
      >
        <div
          className='student_title'
          data-id={this.state.id}
        >
          {this.state.name}
        </div>
        
        {this.state.status ? 
          (<div className={'student_active'}>{Locale.student.statusActive}</div>) : 
          (<div className={'student_fired'}>{Locale.student.statusFired}</div>)
        }
        
        <DeleteIcon
          className='student_delete-icon'
          onClick={(e) => {
            e.stopPropagation();
            if(window.confirm(`${locale.studentFullForm.confirmDeleteMsg} ${this.state.name}?`)) {
              this.deleteStudent();
            }
          }}
        />
      </li>
    )
  }
}

Student.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default withRouter(Student);
