import React from 'react';
import propTypes from 'prop-types';
import classNames from 'classnames';

import './button.scss';

const Button = props => {
  const { className, label, ...otherProps } = props;

  return (
    <button 
      className={classNames('button', className)} 
      {...otherProps}
    >
        {label}
    </button>
  );
};

Button.propTypes = {
  label: propTypes.string
};

Button.defaultProps = {
  label: 'SUBMIT'
};

export default Button;


